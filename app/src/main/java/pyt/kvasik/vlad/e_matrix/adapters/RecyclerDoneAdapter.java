package pyt.kvasik.vlad.e_matrix.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import pyt.kvasik.vlad.e_matrix.R;
import pyt.kvasik.vlad.e_matrix.data.MainDbHelper;
import pyt.kvasik.vlad.e_matrix.data.Note;

public class RecyclerDoneAdapter extends RecyclerView.Adapter<RecyclerDoneAdapter.ViewHolder> {
    private List<Note> mDoneNotesList;
    private MainDbHelper mMainDbHelper;
    private Context mContext;

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView categoryView;
        TextView nameView;
        TextView noteView;
        TextView timeView;
        View mView;

        @SuppressLint("CutPasteId")
        ViewHolder(View v) {
            super(v);
            mView = v;

            categoryView = (TextView) v.findViewById(R.id.category_recycler_item);
            nameView = (TextView) v.findViewById(R.id.name_recycler_item);
            noteView = (TextView) v.findViewById(R.id.note_recycler_item);
            timeView = (TextView) v.findViewById(R.id.time_recycler_item);
        }
    }

    public RecyclerDoneAdapter(List<Note> NoteList, Context context) {
        this.mDoneNotesList = NoteList;
        mMainDbHelper = new MainDbHelper(context);
        mContext = context;
    }

    @Override
    public RecyclerDoneAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_done_item, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final RecyclerDoneAdapter.ViewHolder holder, final int position) {
        holder.nameView.setText(mDoneNotesList.get(position).getName());

        switch(mDoneNotesList.get(position).getCategory()){
            case "red": holder.categoryView.setText(mContext.getString(R.string.hint_red)); break;
            case "orange": holder.categoryView.setText(mContext.getString(R.string.hint_orange)); break;
            case "yellow": holder.categoryView.setText(mContext.getString(R.string.hint_yellow)); break;
            case "green": holder.categoryView.setText(mContext.getString(R.string.hint_green)); break;
        }

        if (!mDoneNotesList.get(position).getNote().equals(""))
            holder.noteView.setText(mDoneNotesList.get(position).getNote());

        //SimpleDateFormat sdf = new SimpleDateFormat("HH:mm MMM dd, yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy, HH:mm");
        Date resultdate = new Date(Long.valueOf(mDoneNotesList.get(position).getTimestamp()));
        holder.timeView.setText(sdf.format(resultdate));
    }

    @Override
    public int getItemCount() {
        return mDoneNotesList.size();
    }
}