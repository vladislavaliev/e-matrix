package pyt.kvasik.vlad.e_matrix.widgets;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.widget.RemoteViews;

import pyt.kvasik.vlad.e_matrix.R;
import pyt.kvasik.vlad.e_matrix.activities.MainActivity;
import pyt.kvasik.vlad.e_matrix.data.MainDbHelper;
import pyt.kvasik.vlad.e_matrix.data.Note;

/**
 * Implementation of App Widget functionality.
 */
public class FullWidget extends AppWidgetProvider {

    @SuppressLint("StaticFieldLeak")
    static Context mContext;
    static RemoteViews views;
    static SharedPreferences sharedPreferences;

    static int[] ids = {R.id.red, R.id.orange, R.id.yellow, R.id.green};

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
        mContext = context;

        views = new RemoteViews(context.getPackageName(), R.layout.full_widget);

        StringBuilder redNotes = new StringBuilder();
        StringBuilder orangeNotes = new StringBuilder();
        StringBuilder yellowNotes = new StringBuilder();
        StringBuilder greenNotes = new StringBuilder();

        for (Note nt : new MainDbHelper(mContext).getAllNotes()) {
            switch (nt.getCategory()) {
                case "red":
                    redNotes.append("• ").append(nt.getName()).append("\n");
                    break;
                case "orange":
                    orangeNotes.append("• ").append(nt.getName()).append("\n");
                    break;
                case "yellow":
                    yellowNotes.append("• ").append(nt.getName()).append("\n");
                    break;
                case "green":
                    greenNotes.append("• ").append(nt.getName()).append("\n");
                    break;
            }
        }

        setWidgetCubsText(redNotes.toString(), orangeNotes.toString(), yellowNotes.toString(),
                greenNotes.toString());

        for (int id : ids)
            views.setOnClickPendingIntent(id, PendingIntent.getActivity(mContext, 0,
                    new Intent(mContext, MainActivity.class), 0));

        loadColors();
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    static void loadColors() {
        sharedPreferences = mContext.getSharedPreferences("colors", Context.MODE_PRIVATE);

        String sRed = sharedPreferences.getString("red", "");
        String sOrange = sharedPreferences.getString("orange", "");
        String sYellow = sharedPreferences.getString("yellow", "");
        String sGreen = sharedPreferences.getString("green", "");

        if (!sRed.equals("")) {
            views.setInt(R.id.red, "setBackgroundColor", Color.parseColor(sRed));
        }
        if (!sOrange.equals("")) {
            views.setInt(R.id.orange, "setBackgroundColor", Color.parseColor(sOrange));
        }
        if (!sYellow.equals("")) {
            views.setInt(R.id.yellow, "setBackgroundColor", Color.parseColor(sYellow));
        }
        if (!sGreen.equals("")) {
            views.setInt(R.id.green, "setBackgroundColor", Color.parseColor(sGreen));
        }
    }

    private static void setWidgetCubsText(String redNotes, String orangeNotes, String yellowNotes,
                                          String greenNotes){
        views.setTextViewText(R.id.red, redNotes);
        views.setTextViewText(R.id.orange, orangeNotes);
        views.setTextViewText(R.id.yellow, yellowNotes);
        views.setTextViewText(R.id.green, greenNotes);
    }
}

