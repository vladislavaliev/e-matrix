package pyt.kvasik.vlad.e_matrix.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import pyt.kvasik.vlad.e_matrix.services.NotifyService;

public class BootBroadcast extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            Intent i = new Intent(context, NotifyService.class);

            if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.O) {
                context.startForegroundService(i);
            }
            else {
                context.startService(i);
            }
        }
    }
}
