package pyt.kvasik.vlad.e_matrix.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.answers.Answers
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.activity_add_note.*
import org.jetbrains.anko.toast
import pyt.kvasik.vlad.e_matrix.R
import pyt.kvasik.vlad.e_matrix.data.MainDbHelper
import pyt.kvasik.vlad.e_matrix.data.Note

class AddNoteActivity : AppCompatActivity(),View.OnClickListener {

    lateinit var db : MainDbHelper

    protected lateinit var categorySpinnerPosition: String
    protected lateinit var alertSpinnerPosition: String
    var categoryString: String? = null
    private var ID: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Fabric.with(this, Crashlytics())
        Fabric.with(this, Answers())

        setContentView(R.layout.activity_add_note)

        db = MainDbHelper(this)

        initActionBar()
        initCategorySpinner()
        initTimeSpinner()

        checkPrevIntent(intent)

        ok.setOnClickListener(this)
    }

    fun initActionBar(){
        val actionBar = supportActionBar!!
        actionBar.setHomeButtonEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true)
    }

    private fun initCategorySpinner(){
        val adapter = ArrayAdapter(this, R.layout.add_note_spinner_item,
                resources.getStringArray(R.array.spinner_strings))
        adapter.setDropDownViewResource(R.layout.add_note_spinner_item)

        category_spin!!.adapter = adapter
        category_spin!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                when (position) {
                    0 -> categorySpinnerPosition = "red"
                    1 -> categorySpinnerPosition = "orange"
                    2 -> categorySpinnerPosition = "yellow"
                    3 -> categorySpinnerPosition = "green"
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    fun initTimeSpinner(){
        val adapter = ArrayAdapter(this, R.layout.add_note_spinner_item,
                resources.getStringArray(R.array.spinner2_strings))
        adapter.setDropDownViewResource(R.layout.add_note_spinner_item)

        time_spin!!.adapter = adapter
        time_spin!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                when (position) {
                    0 -> alertSpinnerPosition = "never"
                    1 -> alertSpinnerPosition = "hour"
                    2 -> alertSpinnerPosition = "day"
                    3 -> alertSpinnerPosition = "month"
                    4 -> alertSpinnerPosition = "year"
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    fun checkPrevIntent(intent : Intent){
        if (intent.getIntExtra("NoteID", -1) != -1) {
            val note = db.getNoteByID(intent.getIntExtra("NoteID", -1))
            ID = note!!.id

            when (note.category) {
                "red" -> category_spin!!.setSelection(0)
                "orange" -> category_spin!!.setSelection(1)
                "yellow" -> category_spin!!.setSelection(2)
                "green" -> category_spin!!.setSelection(3)
            }
            when (note.alerts) {
                "never" -> time_spin!!.setSelection(0)
                "hour" -> time_spin!!.setSelection(1)
                "day" -> time_spin!!.setSelection(2)
                "month" -> time_spin!!.setSelection(3)
                "year" -> time_spin!!.setSelection(4)
            }
            name!!.setText(note.name)
            content!!.setText(note.note)
            ok!!.setText(R.string.change)

            return
        }

        //если кликаем по плитке в MainActivity, то здесь через Extra получаем "Категорию"
        if (intent.getStringExtra("category") != null) {
            categoryString = intent.getStringExtra("category")

            when (categoryString) {
                "red" -> category_spin!!.setSelection(0)
                "orange" -> category_spin!!.setSelection(1)
                "yellow" -> category_spin!!.setSelection(2)
                "green" -> category_spin!!.setSelection(3)
            }
        }
    }

    override fun onClick(p0: View?) {
        val nameText = name!!.text.toString()
        val contentText = content!!.text.toString()
        val buttonText = ok!!.text.toString()

        if (!nameText.isEmpty()) {
            if (buttonText == resources.getString(R.string.change)) {
                db.updateNote(Note(ID, categorySpinnerPosition, alertSpinnerPosition, nameText, contentText))

                toast(getString(R.string.changed))

                finish()
            } else {
                db.addNote(categorySpinnerPosition, alertSpinnerPosition, nameText, contentText)

                toast(getString(R.string.added))

                name!!.setText("")
                content!!.setText("")
            }
        }
    }
}