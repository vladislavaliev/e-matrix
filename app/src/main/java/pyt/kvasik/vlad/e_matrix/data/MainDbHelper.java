package pyt.kvasik.vlad.e_matrix.data;

import android.annotation.SuppressLint;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import java.util.ArrayList;
import java.util.List;

import pyt.kvasik.vlad.e_matrix.widgets.FullWidget;

public class MainDbHelper extends SQLiteOpenHelper implements BaseColumns {

    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "notesManager";
    private static final String MAIN_TABLE = "notes";
    private static final String DONE_TABLE = "done";
    private static final String KEY_ID = "_id";
    private static final String KEY_CATEGORY = "category";
    private static final String KEY_ALERTS = "alerts";
    private static final String KEY_NAME = "name";
    private static final String KEY_NOTES = "note";
    private static final String KEY_TIME = "timestamp";

    private SQLiteDatabase db;
    private Context context;

    public MainDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_NOTES_TABLE = "CREATE TABLE " + MAIN_TABLE + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_CATEGORY + " TEXT," + KEY_ALERTS + " TEXT," + KEY_NAME + " TEXT,"
                + KEY_NOTES + " TEXT," + KEY_TIME + " TEXT" + ");";

        db.execSQL(CREATE_NOTES_TABLE);
        createDoneTable(db);
    }

    void createDoneTable(SQLiteDatabase db) {
        String CREATE_DONE_TABLE = "CREATE TABLE " + DONE_TABLE + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_CATEGORY + " TEXT," + KEY_NAME + " TEXT,"
                + KEY_NOTES + " TEXT," + KEY_TIME + " TEXT);";
        db.execSQL(CREATE_DONE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        createDoneTable(db);
    }

    public void addNote(String category, String alerts, String name, String note) {
        db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_CATEGORY, category);
        values.put(KEY_ALERTS, alerts);
        values.put(KEY_NAME, name);
        values.put(KEY_NOTES, note);
        values.put(KEY_TIME, String.valueOf(System.currentTimeMillis()));

        // Inserting Row
        db.insert(MAIN_TABLE, null, values);
        updateWidget();
        db.close(); // Closing database connection
    }

    public void updateWidget() {
        int[] ids = AppWidgetManager.getInstance(context).getAppWidgetIds(new ComponentName(context, FullWidget.class));
        new FullWidget().onUpdate(context, AppWidgetManager.getInstance(context), ids);
    }

    public List<Note> getNotesByCategory(String category) {
        db = this.getReadableDatabase();
        List<Note> noteList = new ArrayList<>();

        @SuppressLint("Recycle")
        Cursor cursor = db.rawQuery("SELECT * FROM " + MAIN_TABLE + " WHERE category = '" + category + "'", null);

        if (cursor.moveToFirst()) {
            do {
                Note note = new Note();
                note.setId(Integer.parseInt(cursor.getString(0)));
                note.setCategory(cursor.getString(1));
                note.setAlerts(cursor.getString(2));
                note.setName(cursor.getString(3));
                note.setNote(cursor.getString(4));

                noteList.add(note);
            } while (cursor.moveToNext());
        }

        db.close();
        return noteList;
    }

    public Note getNoteByID(int id) {
        db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + MAIN_TABLE + " WHERE _id = " + id, null);
        Note note = null;
        if (cursor.moveToFirst()) {
            do {
                note = new Note();
                note.setId(Integer.parseInt(cursor.getString(0)));
                note.setCategory(cursor.getString(1));
                note.setAlerts(cursor.getString(2));
                note.setName(cursor.getString(3));
                note.setNote(cursor.getString(4));

            } while (cursor.moveToNext());
        }
        //db.close();
        return note;
    }

    public List<Note> getAllNotes() {
        List<Note> noteList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + MAIN_TABLE;

        db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Note note = new Note();
                note.setId(Integer.parseInt(cursor.getString(0)));
                note.setCategory(cursor.getString(1));
                note.setAlerts(cursor.getString(2));
                note.setName(cursor.getString(3));
                note.setNote(cursor.getString(4));
                note.setTimestamp(cursor.getString(5));

                noteList.add(note);
            } while (cursor.moveToNext());
        }

        db.close();
        return noteList;
    }

    public void updateNote(Note note) {
        db = this.getWritableDatabase();

        Long tsLong = System.currentTimeMillis();

        ContentValues values = new ContentValues();
        values.put(KEY_CATEGORY, note.getCategory());
        values.put(KEY_ALERTS, note.getAlerts());
        values.put(KEY_NAME, note.getName());
        values.put(KEY_NOTES, note.getNote());
        values.put(KEY_TIME, tsLong.toString());

        db.update(MAIN_TABLE, values, KEY_ID + " = ?",
                new String[]{String.valueOf(note.getId())});
        updateWidget();
        db.close();
    }

    public void deleteNote(int id) {
        db = this.getWritableDatabase();
        db.delete(MAIN_TABLE, KEY_ID + " = ?",
                new String[]{String.valueOf(id)});
        updateWidget();
        db.close();
    }

    public void deleteDoneNote(int id) {
        db = this.getWritableDatabase();
        db.delete(DONE_TABLE, KEY_ID + " = ?",
                new String[]{String.valueOf(id)});
        db.close();
    }

    public void addDoneNote(int id) {
        db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_CATEGORY, getNoteByID(id).getCategory());
        values.put(KEY_NAME, getNoteByID(id).getName());
        values.put(KEY_NOTES, getNoteByID(id).getNote());
        values.put(KEY_TIME, String.valueOf(System.currentTimeMillis()));

        // Inserting Row
        db.insert(DONE_TABLE, null, values);
        db.close();
    }

    public List<Note> getAllDoneNotes() {
        List<Note> noteList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + DONE_TABLE;

        db = this.getWritableDatabase();
        @SuppressLint("Recycle")
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Note note = new Note();
                note.setId(Integer.parseInt(cursor.getString(0)));
                note.setCategory(cursor.getString(1));
                note.setName(cursor.getString(2));
                note.setNote(cursor.getString(3));
                note.setTimestamp(cursor.getString(4));

                noteList.add(note);
            } while (cursor.moveToNext());
        }

        db.close();
        return noteList;
    }

    public int getNotesCountByCategory(String category) {
        int count;
        db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + MAIN_TABLE + " WHERE category = '" + category + "'", null);
        count = cursor.getCount();

        cursor.close();
        db.close();

        return count;
    }
}