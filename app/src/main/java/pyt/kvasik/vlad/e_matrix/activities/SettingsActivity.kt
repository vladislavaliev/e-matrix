package pyt.kvasik.vlad.e_matrix.activities

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.answers.Answers
import com.google.android.gms.ads.AdRequest
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.activity_settings.*
import pyt.kvasik.vlad.e_matrix.R
import pyt.kvasik.vlad.e_matrix.utils.Constants

class SettingsActivity : AppCompatActivity() {

    private lateinit var sharedPreferences: SharedPreferences

    private lateinit var loadLoginString: String
    private lateinit var loadPasswordString: String
    private var onGuard: Boolean = false

    private val DIALOG_ABOUT = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Fabric.with(this, Crashlytics())
        Fabric.with(this, Answers())

        setContentView(R.layout.activity_settings)

        val actionBar = supportActionBar!!
        actionBar.setHomeButtonEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true)

        sharedPreferences = getSharedPreferences(Constants.USER_INFO_PREF, Context.MODE_PRIVATE)

        loadText()

        name!!.setText(loadLoginString)
        password!!.setText(loadPasswordString)

        loadGuard()
        if (!onGuard)
            onguard!!.isChecked = true

        initListeners()

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        adView.loadAd(AdRequest.Builder().build())
    }

    override fun onDestroy() {
        super.onDestroy()

        if (password!!.text.toString().isNotEmpty()) {
            saveText()
            saveGuard()
        }
    }

    private fun saveText() {
        val editor = sharedPreferences.edit()
        editor.putString(Constants.USER_NAME_PREF, name!!.text.toString())
        editor.putString(Constants.USER_PASSWORD_PREF, password!!.text.toString())
        editor.apply()
    }

    private fun loadText() {
        loadLoginString = sharedPreferences.getString(Constants.USER_NAME_PREF, "")!!
        loadPasswordString = sharedPreferences.getString(Constants.USER_PASSWORD_PREF, "")!!
    }

    private fun saveGuard() {
        val editor = sharedPreferences.edit()
        editor.putBoolean(Constants.USER_GUARD_ON_PREF, onGuard)
        editor.apply()
    }

    private fun loadGuard() {
        onGuard = sharedPreferences.getBoolean(Constants.USER_GUARD_ON_PREF, onGuard)
    }

    private fun initListeners(){
        rate_button.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(Constants.PLAY_MARKET_URI)
            try {
                startActivity(intent)
            } catch (e: Exception) {
                Toast.makeText(this,
                        getString(R.string.rate_error),
                        Toast.LENGTH_SHORT).show()
            }
        }

        onguard.setOnClickListener {
            onGuard = !onGuard
            saveGuard()
        }

        button_about.setOnClickListener {
            showDialog(DIALOG_ABOUT)
        }
    }

    override fun onCreateDialog(id: Int): Dialog {
        if (id == DIALOG_ABOUT) {
            val adb = AlertDialog.Builder(this)
            adb.setTitle(R.string.about)
            adb.setMessage(resources.getString(R.string.app_name) + ", " + resources.getString(R.string.version)
                    + "\n" + resources.getString(R.string.e_mail))

            return adb.create()
        }
        return super.onCreateDialog(id)
    }
}