package pyt.kvasik.vlad.e_matrix.services;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.Nullable;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pyt.kvasik.vlad.e_matrix.activities.MainActivity;
import pyt.kvasik.vlad.e_matrix.data.MainDbHelper;
import pyt.kvasik.vlad.e_matrix.data.Note;

public class NotifyService extends Service {

    MainDbHelper db;
    private String categoryString;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Timer myTimer = new Timer(); // Создаем таймер

        db = new MainDbHelper(this);
        myTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                db = new MainDbHelper(getApplicationContext());
                List<Note> noteList = db.getAllNotes();

                for (Note nt : noteList) {
                    long curTime = System.currentTimeMillis();
                    long dbTime = Long.parseLong(nt.getTimestamp());

                    String timeFormat;
                    long interval;

                    switch (nt.getAlerts()) {
                        case "hour":
                            timeFormat = "HH";
                            interval = 1L;
                            break;
                        case "day":
                            timeFormat = "dd";
                            interval = 1L;
                            break;
                        case "month":
                            timeFormat = "MM";
                            interval = 1L;
                            break;
                        case "year":
                            timeFormat = "yyyy";
                            interval = 1L;
                            break;
                        default:
                            continue;
                    }

                    DateFormat formatter = new SimpleDateFormat(timeFormat);

                    String curTimeStr = formatter.format(new Date(curTime));
                    String dbTimeStr = formatter.format(new Date(dbTime));

                    Long tmp = Long.valueOf(dbTimeStr) + interval;

                    Long tmp2 = Long.valueOf(curTimeStr);

                    if (tmp2 >= tmp) {
                        db.updateNote(nt);
                        notifyFunction(nt);
                    }
                }
            }
        }, 0L, 300L * 1000); // (сейчас каждые 5 мин)

        return Service.START_STICKY;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void notifyFunction(Note note) {
        Context context = getApplicationContext();

        Intent notificationIntent = new Intent(context, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(context,
                0, notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        Resources res = context.getResources();
        Notification.Builder builder = new Notification.Builder(context);

        switch (note.getCategory()) {
            case "red":
                categoryString = getResources().getString(pyt.kvasik.vlad.e_matrix.R.string.hint_red);
                break;
            case "orange":
                categoryString = getResources().getString(pyt.kvasik.vlad.e_matrix.R.string.hint_orange);
                break;
            case "yellow":
                categoryString = getResources().getString(pyt.kvasik.vlad.e_matrix.R.string.hint_yellow);
                break;
            case "green":
                categoryString = getResources().getString(pyt.kvasik.vlad.e_matrix.R.string.hint_green);
                break;
        }

        builder.setContentIntent(contentIntent)
                .setSmallIcon(pyt.kvasik.vlad.e_matrix.R.drawable.ic_notif_icon)
                // большая картинка
                .setLargeIcon(BitmapFactory.decodeResource(res, pyt.kvasik.vlad.e_matrix.R.mipmap.ic_launcher))
                // текст в строке состояния
                .setTicker(note.getCategory())// Превью
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                // Заголовок уведомления
                .setContentTitle(categoryString)
                // Текст уведомления
                .setContentText(note.getName());

        // Notification notification = builder.getNotification(); // до API 16
        Notification notification = builder.build();
        notification.defaults = Notification.DEFAULT_ALL;

        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(note.getId(), notification);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
