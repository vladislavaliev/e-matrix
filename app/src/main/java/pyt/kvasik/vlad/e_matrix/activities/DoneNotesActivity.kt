package pyt.kvasik.vlad.e_matrix.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.answers.Answers
import com.github.brnunes.swipeablerecyclerview.SwipeableRecyclerViewTouchListener
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.done_recycle_view.*
import org.jetbrains.anko.toast
import pyt.kvasik.vlad.e_matrix.R
import pyt.kvasik.vlad.e_matrix.adapters.RecyclerDoneAdapter
import pyt.kvasik.vlad.e_matrix.data.MainDbHelper
import pyt.kvasik.vlad.e_matrix.data.Note

class DoneNotesActivity : AppCompatActivity() {

    private var mAdapter: RecyclerDoneAdapter? = null
    private var noteDoneList: MutableList<Note>? = null

    internal lateinit var db: MainDbHelper

    override fun onResume() {
        super.onResume()

        DBworking()
    }

    private fun DBworking() {
        noteDoneList = db.allDoneNotes

        if(noteDoneList!!.isEmpty())
            stateful_view.showEmpty()
        else {
            mAdapter = RecyclerDoneAdapter(noteDoneList, this)
            my_recycler_view!!.adapter = mAdapter
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Fabric.with(this, Crashlytics())
        Fabric.with(this, Answers())

        setContentView(R.layout.done_recycle_view)

        db = MainDbHelper(this)

        assert(my_recycler_view != null)
        my_recycler_view!!.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(this)
        my_recycler_view!!.layoutManager = layoutManager

        val swipeTouchListener = SwipeableRecyclerViewTouchListener(my_recycler_view!!,
                object : SwipeableRecyclerViewTouchListener.SwipeListener {

                    override fun canSwipeLeft(position: Int): Boolean {
                        return true
                    }

                    override fun canSwipeRight(position: Int): Boolean {
                        return true
                    }

                    override fun onDismissedBySwipeLeft(recyclerView: RecyclerView, reverseSortedPositions: IntArray) {
                        onDismissedBySwipeRight(recyclerView, reverseSortedPositions)
                    }

                    override fun onDismissedBySwipeRight(recyclerView: RecyclerView, reverseSortedPositions: IntArray) {
                        for (position in reverseSortedPositions) {
                            db.deleteDoneNote(noteDoneList!![position].id)
                            noteDoneList!!.removeAt(position)
                            mAdapter!!.notifyItemRemoved(position)

                            if(noteDoneList!!.isEmpty())
                                stateful_view.showEmpty()

                            toast(applicationContext.getString(R.string.deleted))
                        }
                        mAdapter!!.notifyDataSetChanged()
                    }
                })

        my_recycler_view!!.addOnItemTouchListener(swipeTouchListener)
    }
}
