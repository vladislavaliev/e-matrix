package pyt.kvasik.vlad.e_matrix.data;

public class Note {

    //private variables
    private int _id;
    private String _category;
    private String _alerts;
    private String _name;
    private String _note;
    private String _timestamp;

    // Empty constructor
    public Note(){}

    // constructor
    public Note(int id, String category, String alerts,String name,String note){
        this._id = id;
        this._category = category;
        this._alerts = alerts;
        this._name = name;
        this._note = note;
    }

    // constructor
    public Note(String name, String note){
        this._name = name;
        this._note = note;
    }

    public int getId(){
        return this._id;
    }

    public void setId(int id){
        this._id = id;
    }

    public String getCategory(){
        return this._category;
    }

    public void setCategory(String category){
        this._category = category;
    }

    public String getAlerts(){
        return this._alerts;
    }

    public void setAlerts(String alerts){
        this._alerts = alerts;
    }

    public String getName(){
        return this._name;
    }

    public void setName(String name){
        this._name = name;
    }

    public String getNote(){
        return this._note;
    }

    public void setNote(String note){
        this._note = note;
    }

    public String getTimestamp() {
        return _timestamp;
    }

    public void setTimestamp(String _timestamp) {
        this._timestamp = _timestamp;
    }
}

