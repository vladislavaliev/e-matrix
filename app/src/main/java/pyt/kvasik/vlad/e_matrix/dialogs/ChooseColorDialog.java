package pyt.kvasik.vlad.e_matrix.dialogs;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import pyt.kvasik.vlad.e_matrix.R;
import pyt.kvasik.vlad.e_matrix.data.MainDbHelper;

import static android.content.Context.MODE_PRIVATE;

@SuppressLint("ValidFragment")
public class ChooseColorDialog extends DialogFragment implements View.OnClickListener {

    private String mCategory;

    private ChooseColorDialogListener listener;

    @SuppressLint("ValidFragment")
    public ChooseColorDialog(String category) {
        mCategory = category;
    }

    public void setListener(ChooseColorDialogListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View form = getActivity().getLayoutInflater().inflate(R.layout.choose_colors_dialog, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        Button redBtn = form.findViewById(R.id.red);
        Button orangeBtn = form.findViewById(R.id.orange);
        Button yellowBtn = form.findViewById(R.id.yellow);
        Button greenBtn = form.findViewById(R.id.green);
        Button greyBtn = form.findViewById(R.id.grey);
        Button blueBtn = form.findViewById(R.id.blue);

        redBtn.setOnClickListener(this);
        orangeBtn.setOnClickListener(this);
        yellowBtn.setOnClickListener(this);
        greenBtn.setOnClickListener(this);
        greyBtn.setOnClickListener(this);
        blueBtn.setOnClickListener(this);

        return (builder.setTitle(getString(R.string.change_colors)).setView(form).create());
    }

    @Override
    public void onCancel(@NonNull DialogInterface unused) {
        super.onCancel(unused);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.red:
                saveColors("#FF9C9C");
                break;
            case R.id.orange:
                saveColors("#FFC69D");
                break;
            case R.id.yellow:
                saveColors("#FDFF9C");
                break;
            case R.id.green:
                saveColors("#BDFFA2");
                break;
            case R.id.grey:
                saveColors("#DFDFDF");
                break;
            case R.id.blue:
                saveColors("#BACEFF");
                break;
        }

        new MainDbHelper(getContext()).updateWidget();

        listener.onColorChanged();

        dismiss();
    }

    private void saveColors(String selColor) {
        if(getActivity() == null)
            return;

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("colors",
                MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(mCategory, selColor);
        editor.apply();
    }

    public interface ChooseColorDialogListener {

        void onColorChanged();
    }
}