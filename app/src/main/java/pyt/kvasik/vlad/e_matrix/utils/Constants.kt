package pyt.kvasik.vlad.e_matrix.utils

/**
 * Created by vladaliev on 08.12.2017.
 */
class Constants {

    companion object {

        const val USER_INFO_PREF: String = "user_info"
        const val USER_PASSWORD_PREF: String = "user_password"
        const val USER_NAME_PREF: String = "user_name"
        const val USER_GUARD_ON_PREF: String = "user_guard_on"
        const val USER_COLORS_PREF: String = "colors"

        const val EXTRA_COLOR : String = "color"

        const val RED_CUBE = "red"
        const val ORANGE_CUBE = "orange"
        const val YELLOW_CUBE = "yellow"
        const val GREEN_CUBE = "green"

        const val PLAY_MARKET_URI: String = "market://details?id=pic.kvasik.vlad.e_matrix"
    }
}