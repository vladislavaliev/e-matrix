package pyt.kvasik.vlad.e_matrix.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.answers.Answers
import io.fabric.sdk.android.Fabric

/**
 * Created by vladaliev on 24.01.2018.
 */

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Fabric.with(this, Crashlytics())
        Fabric.with(this, Answers())

        startActivity(Intent(this, SignInActivity::class.java))
        finish()
    }
}