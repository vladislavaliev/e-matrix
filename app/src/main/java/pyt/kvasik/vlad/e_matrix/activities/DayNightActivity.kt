package pyt.kvasik.vlad.e_matrix.activities

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import pyt.kvasik.vlad.e_matrix.R
import pyt.kvasik.vlad.e_matrix.utils.DecorUtil
import pyt.kvasik.vlad.e_matrix.utils.DecorUtil.Companion.checkSystemDarkModeActive

@SuppressLint("Registered")
open class DayNightActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        DecorUtil.setStatusBarColor(checkSystemDarkModeActive(this), window, this)
    }
}