package pyt.kvasik.vlad.e_matrix.activities

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.answers.Answers
import com.google.android.material.navigation.NavigationView
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import pyt.kvasik.vlad.e_matrix.R
import pyt.kvasik.vlad.e_matrix.data.MainDbHelper
import pyt.kvasik.vlad.e_matrix.dialogs.ChooseColorDialog
import pyt.kvasik.vlad.e_matrix.utils.Constants
import pyt.kvasik.vlad.e_matrix.utils.DecorUtil

class MainActivity : AppCompatActivity(),
        NavigationView.OnNavigationItemSelectedListener, View.OnClickListener,
        View.OnLongClickListener, ChooseColorDialog.ChooseColorDialogListener {

    private var colorsPanel = arrayOfNulls<TextView>(4)
    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Fabric.with(this, Crashlytics())
        Fabric.with(this, Answers())

        setContentView(R.layout.activity_main)

        DecorUtil.setStatusBarColor(window, this, R.color.main_page_bg_color)

        colorsPanel[0] = red
        colorsPanel[1] = orange
        colorsPanel[2] = yellow
        colorsPanel[3] = green

        for (i in 0..3) {
            colorsPanel[i]!!.movementMethod = ScrollingMovementMethod()
            colorsPanel[i]!!.setOnLongClickListener(this)
        }

        initDrawer()
        initListeners()

        dataBaseWork()
    }

    override fun onResume() {
        super.onResume()

        loadColors()
        dataBaseWork()
    }

    private fun initDrawer() {
        setSupportActionBar(toolbar)
        toolbar.title = getString(R.string.main_screen)

        val toggle = ActionBarDrawerToggle(this, drawer_layout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_done -> startActivity(Intent(this, DoneNotesActivity::class.java))
            R.id.nav_settings -> startActivity(Intent(this, SettingsActivity::class.java))
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun dataBaseWork() {
        for (i in 0..3)
            colorsPanel[i]!!.text = ""

        val db = MainDbHelper(this)
        val noteList = db.allNotes
        for (nt in noteList) {
            when (nt.category) {
                Constants.RED_CUBE -> colorsPanel[0]!!.append("• " + nt.name + "\n")
                Constants.ORANGE_CUBE -> colorsPanel[1]!!.append("• " + nt.name + "\n")
                Constants.YELLOW_CUBE -> colorsPanel[2]!!.append("• " + nt.name + "\n")
                Constants.GREEN_CUBE -> colorsPanel[3]!!.append("• " + nt.name + "\n")
            }
        }
    }

    private fun initListeners() {
        red.setOnClickListener(this)
        orange.setOnClickListener(this)
        yellow.setOnClickListener(this)
        green.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.red -> startActivity(Intent(this, NotesListActivity::class.java).putExtra(Constants.EXTRA_COLOR, Constants.RED_CUBE))
            R.id.orange -> startActivity(Intent(this,
                    NotesListActivity::class.java).putExtra(Constants.EXTRA_COLOR, Constants.ORANGE_CUBE))
            R.id.yellow -> startActivity(Intent(this,
                    NotesListActivity::class.java).putExtra(Constants.EXTRA_COLOR, Constants.YELLOW_CUBE))
            R.id.green -> startActivity(Intent(this,
                    NotesListActivity::class.java).putExtra(Constants.EXTRA_COLOR, Constants.GREEN_CUBE))
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.add -> startActivity(Intent(this, AddNoteActivity::class.java))
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onLongClick(v: View): Boolean {
        var category: String? = null

        when (v.id) {
            R.id.red -> category = Constants.RED_CUBE
            R.id.orange -> category = Constants.ORANGE_CUBE
            R.id.yellow -> category = Constants.YELLOW_CUBE
            R.id.green -> category = Constants.GREEN_CUBE
        }

        val dialog = ChooseColorDialog(category)
        dialog.setListener(this)
        dialog.show(supportFragmentManager, null)

        return false
    }

    private fun loadColors() {
        sharedPreferences = getSharedPreferences(Constants.USER_COLORS_PREF, Context.MODE_PRIVATE)

        if (sharedPreferences.getString(Constants.RED_CUBE, "") != "") {
            colorsPanel[0]!!.setBackgroundColor(
                    Color.parseColor(sharedPreferences.getString(Constants.RED_CUBE, "")))
        }
        if (sharedPreferences.getString(Constants.ORANGE_CUBE, "") != "") {
            colorsPanel[1]!!.setBackgroundColor(
                    Color.parseColor(sharedPreferences.getString(Constants.ORANGE_CUBE, "")))
        }
        if (sharedPreferences.getString(Constants.YELLOW_CUBE, "") != "") {
            colorsPanel[2]!!.setBackgroundColor(
                    Color.parseColor(sharedPreferences.getString(Constants.YELLOW_CUBE, "")))
        }
        if (sharedPreferences.getString(Constants.GREEN_CUBE, "") != "") {
            colorsPanel[3]!!.setBackgroundColor(
                    Color.parseColor(sharedPreferences.getString(Constants.GREEN_CUBE, "")))
        }
    }

    override fun onColorChanged() {
        loadColors()
    }
}
