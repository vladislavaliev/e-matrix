package pyt.kvasik.vlad.e_matrix.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView

import android.widget.TextView
import androidx.core.content.ContextCompat

import com.google.android.material.snackbar.Snackbar
import com.google.android.material.snackbar.Snackbar.SnackbarLayout
import pyt.kvasik.vlad.e_matrix.R


class Util {

    companion object {

        fun showSnackbar(context: Context, containerView : View,
                         message : String, duration : Int) {

            val snackbar = Snackbar.make(containerView, message, duration)

            snackbar.view.setBackgroundColor(ContextCompat.getColor(context,
                    R.color.accent))

            snackbar.setTextColor(ContextCompat.getColor(context,
                    R.color.snackbar_text_color))

            snackbar.setActionTextColor(ContextCompat.getColor(context,
                    R.color.accent))
            .show()
        }

    }
}