package pyt.kvasik.vlad.e_matrix.activities

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.answers.Answers
import com.google.android.gms.ads.AdRequest
import com.google.android.material.snackbar.Snackbar
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.sign_in_activity.*
import pyt.kvasik.vlad.e_matrix.R
import pyt.kvasik.vlad.e_matrix.services.NotifyService
import pyt.kvasik.vlad.e_matrix.utils.Constants
import pyt.kvasik.vlad.e_matrix.utils.Util


/**
 * Created by vladaliev on 08.12.2017.
 */
class SignInActivity : DayNightActivity(), View.OnClickListener {

    private lateinit var sharedPreferences: SharedPreferences
    private var loadLoginString: String? = null
    private var loadPasswordString: String? = null
    private var onGuard: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Fabric.with(this, Crashlytics())
        Fabric.with(this, Answers())

        setContentView(R.layout.sign_in_activity)

        this.stopService(Intent(this, NotifyService::class.java))
        this.startService(Intent(this, NotifyService::class.java))

        init()
    }

    private fun init() {

        toolbar.title = getString(R.string.enter_in_app)
        setSupportActionBar(toolbar)

        sharedPreferences = getSharedPreferences(Constants.USER_INFO_PREF, Context.MODE_PRIVATE)
        loadGuard()
        loadText()
        initAds()

        if (loadLoginString!!.isNotEmpty()) {
            tvDescription.text = getString(R.string.for_enter_in_app_need_pass)
            login.setText(loadLoginString)
            login.isEnabled = false
            signButton.text = getString(R.string.title_activity_login)

            password.addTextChangedListener(object : TextWatcher {

                override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

                override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

                override fun afterTextChanged(editable: Editable) {
                    if (!loadPasswordString.isNullOrEmpty()) {
                        if (loadPasswordString.equals(editable.toString()))
                            onClick(signButton)
                    }
                }
            })
        }

        password.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                onClick(signButton)
                true
            } else false
        }

        signButton.setOnClickListener(this)
    }

    private fun initAds() {
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }

    override fun onClick(v: View) {
        if (login.text.toString().isEmpty()) {
            Util.showSnackbar(this, llMain, getString(R.string.enter_name),
                    Snackbar.LENGTH_SHORT)
            return
        }

        if (password.text.toString().isEmpty()) {
            Util.showSnackbar(this, llMain, getString(R.string.enter_password),
                    Snackbar.LENGTH_SHORT)
            return
        }

        if (loadPasswordString!!.isEmpty() || password.text.toString() == loadPasswordString) {
            saveText()
            startActivity(Intent(this, MainActivity::class.java))
            this.finish()
        } else {
            Util.showSnackbar(this, llMain, getString(R.string.incorrect_pas),
                    Snackbar.LENGTH_SHORT)
        }
    }

    private fun saveText() {
        val editor = sharedPreferences.edit()
        editor.putString(Constants.USER_NAME_PREF, login.text.toString())
        editor.putString(Constants.USER_PASSWORD_PREF, password.text.toString())
        editor.apply()
    }

    private fun loadText() {
        loadLoginString = sharedPreferences.getString(Constants.USER_NAME_PREF, "")
        loadPasswordString = sharedPreferences.getString(Constants.USER_PASSWORD_PREF, "")
    }

    private fun loadGuard() {
        onGuard = sharedPreferences.getBoolean(Constants.USER_GUARD_ON_PREF, onGuard)

        if (onGuard) {
            startActivity(Intent(this, MainActivity::class.java))
            this.finish()
        }
    }
}