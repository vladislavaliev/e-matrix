package pyt.kvasik.vlad.e_matrix.utils

import android.content.Context
import android.content.res.Configuration
import android.os.Build
import android.view.View
import android.view.Window
import androidx.core.content.ContextCompat
import pyt.kvasik.vlad.e_matrix.R

class DecorUtil {

    companion object {

        fun checkSystemDarkModeActive(context: Context): Boolean {
            val nightModeFlag = context.resources.configuration.uiMode and
                    Configuration.UI_MODE_NIGHT_MASK

            return nightModeFlag == Configuration.UI_MODE_NIGHT_YES
        }

        fun setStatusBarColor(isDarkStatusBar: Boolean, window : Window, context: Context) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

//            if (isDarkStatusBar) {
//                window.decorView.systemUiVisibility = 0
//                window.statusBarColor = ContextCompat.getColor(this,
//                        R.color.dark_status_bar_color)
//            } else {
                window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                window.statusBarColor = ContextCompat.getColor(context,
                        R.color.light_status_bar_color)
                //   }
            }
        }

        fun setStatusBarColor(window : Window, context: Context, color : Int) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                window.statusBarColor = ContextCompat.getColor(context, color)
            }
        }
    }
}