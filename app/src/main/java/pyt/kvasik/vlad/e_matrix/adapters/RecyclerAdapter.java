package pyt.kvasik.vlad.e_matrix.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cz.kinst.jakub.view.SimpleStatefulLayout;
import pyt.kvasik.vlad.e_matrix.R;
import pyt.kvasik.vlad.e_matrix.activities.AddNoteActivity;
import pyt.kvasik.vlad.e_matrix.data.MainDbHelper;
import pyt.kvasik.vlad.e_matrix.data.Note;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
    private List<Note> mNoteList;
    private MainDbHelper mMainDbHelper;
    private SimpleStatefulLayout mStatefulLayout;
    private String alerts;

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView nameTextView;
        TextView alertTextView;
        TextView noteTextView;
        View mView;
        ImageView doneBtn;

        ViewHolder(View v) {
            super(v);
            mView = v;

            nameTextView = v.findViewById(R.id.name_recycler_item);
            alertTextView = v.findViewById(R.id.alert_recycler_item);
            noteTextView = v.findViewById(R.id.note_recycler_item);

            doneBtn = v.findViewById(R.id.img_done);
        }
    }

    public RecyclerAdapter(List<Note> mNoteList, SimpleStatefulLayout statefulLayout,
                           Context context) {
        this.mNoteList = mNoteList;
        mMainDbHelper = new MainDbHelper(context);
        mStatefulLayout = statefulLayout;
    }

    @Override
    public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item,
                parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final RecyclerAdapter.ViewHolder holder, final int position) {
        final Context context = holder.mView.getContext();

        holder.nameTextView.setText(mNoteList.get(position).getName());

        switch (mNoteList.get(position).getAlerts()) {
            case "never":
                alerts = context.getString(R.string.never);
                break;
            case "hour":
                alerts = context.getString(R.string.every_hour);
                break;
            case "day":
                alerts = context.getString(R.string.once_a_day);
                break;
            case "month":
                alerts = context.getString(R.string.once_a_month);
                break;
            case "year":
                alerts = context.getString(R.string.once_a_year);
                break;
        }

        holder.alertTextView.setText(alerts);
        holder.noteTextView.setText(mNoteList.get(position).getNote());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddNoteActivity.class);
                intent.putExtra("NoteID", mNoteList.get(position).getId());
                context.startActivity(intent);
            }
        });

        holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                createShareDialog(position, context);

                return false;
            }
        });

        holder.doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMainDbHelper.addDoneNote(mNoteList.get(position).getId());
                Toast.makeText(context, context.getString(R.string.done_notes),
                        Toast.LENGTH_SHORT).show();
                remove(position);
            }
        });
    }

    private void createShareDialog(final int position, final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.share)).setPositiveButton(
                context.getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT,
                                mNoteList.get(position).getName() + "\n\n"
                                        + mNoteList.get(position).getNote());
                        sendIntent.setType("text/plain");
                        context.startActivity(sendIntent);
                    }
                }).setNegativeButton(context.getString(R.string.no), null).show();
    }

    private void remove(int position) {
        mMainDbHelper.deleteNote(mNoteList.get(position).getId());
        mNoteList.remove(position);

        if (mNoteList.isEmpty())
            mStatefulLayout.showEmpty();

        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mNoteList.size());
    }

    @Override
    public int getItemCount() {
        return mNoteList.size();
    }
}