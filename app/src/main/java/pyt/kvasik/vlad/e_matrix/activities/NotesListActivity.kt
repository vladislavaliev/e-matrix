package pyt.kvasik.vlad.e_matrix.activities

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.answers.Answers
import com.github.brnunes.swipeablerecyclerview.SwipeableRecyclerViewTouchListener
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.recycle_view.*
import org.jetbrains.anko.toast
import pyt.kvasik.vlad.e_matrix.R
import pyt.kvasik.vlad.e_matrix.adapters.RecyclerAdapter
import pyt.kvasik.vlad.e_matrix.data.MainDbHelper
import pyt.kvasik.vlad.e_matrix.data.Note

class NotesListActivity : AppCompatActivity(), View.OnClickListener {

    private var categoryString: String? = null
    private var mAdapter: RecyclerAdapter? = null
    private var noteList: MutableList<Note>? = null

    private lateinit var hideButton: Animation
    private lateinit var appearButton: Animation

    internal lateinit var db: MainDbHelper

    override fun onResume() {
        super.onResume()

        if (db.getNotesCountByCategory(categoryString) == 0)
            finish()
        else
            dbWork()
    }

    private fun dbWork() {
        stateful_view.showContent()

        noteList = db.getNotesByCategory(categoryString)
        mAdapter = RecyclerAdapter(noteList, stateful_view, this)
        my_recycler_view!!.adapter = mAdapter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Fabric.with(this, Crashlytics())
        Fabric.with(this, Answers())

        setContentView(R.layout.recycle_view)

        db = MainDbHelper(applicationContext)

        val intent = intent
        categoryString = intent.getStringExtra("color")

        when (categoryString) {
            "red" -> supportActionBar!!.setTitle(R.string.hint_red)
            "orange" -> supportActionBar!!.setTitle(R.string.hint_orange)
            "yellow" -> supportActionBar!!.setTitle(R.string.hint_yellow)
            "green" -> supportActionBar!!.setTitle(R.string.hint_green)
        }

        my_recycler_view!!.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(this)
        my_recycler_view!!.layoutManager = layoutManager

        val swipeTouchListener = SwipeableRecyclerViewTouchListener(my_recycler_view!!,
                object : SwipeableRecyclerViewTouchListener.SwipeListener {
                    override fun canSwipeLeft(position: Int): Boolean {
                        return true
                    }

                    override fun canSwipeRight(position: Int): Boolean {
                        return true
                    }

                    override fun onDismissedBySwipeLeft(recyclerView: RecyclerView,
                                                        reverseSortedPositions: IntArray) {
                        onDismissedBySwipeRight(recyclerView, reverseSortedPositions)
                    }

                    override fun onDismissedBySwipeRight(recyclerView: RecyclerView,
                                                         reverseSortedPositions: IntArray) {
                        for (position in reverseSortedPositions) {
                            db.deleteNote(noteList!![position].id)
                            noteList!!.removeAt(position)
                            mAdapter!!.notifyItemRemoved(position)

                            if (noteList!!.isEmpty())
                                stateful_view.showEmpty()

                            toast(getString(R.string.deleted))
                        }
                        mAdapter!!.notifyDataSetChanged()
                    }
                })

        my_recycler_view!!.addOnItemTouchListener(swipeTouchListener)

        my_recycler_view!!.setOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                if (dy > 0)
                    hideButton()
                else
                    appearButton()
            }
        })

        //dbWork()

        fab!!.setOnClickListener(this)

        //Animations
        hideButton = AnimationUtils.loadAnimation(application, R.anim.fab_hide)
        appearButton = AnimationUtils.loadAnimation(application, R.anim.fab_show)

        if (db.getNotesByCategory(categoryString).size == 0)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1)
                fab!!.callOnClick()
    }

    override fun onClick(v: View) {
        val intent = Intent(this, AddNoteActivity::class.java)
        intent.putExtra("category", categoryString)
        startActivity(intent)
    }

    private fun hideButton() {
        fab!!.startAnimation(hideButton)
        fab!!.isClickable = false
    }

    private fun appearButton() {
        fab!!.startAnimation(appearButton)
        fab!!.isClickable = true
    }
}
